require('datejs')
var oracledb = require('oracledb');
var bodyParser = require('body-parser');
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var apn = require('apn');
var request = require('request');
var dashboard_poll_interval = 500;
var order_poll_interval = 500;
var emit_interval = 60;
var dbConnection;
var last_dashboard_id = 100000;
var should_poll_dashboard = true;
var last_order_id = 0;
var timeout = 0;
var dashboardEvents = [];
var debug = true;

server.listen(3000);

oracledb.getConnection({
        user: "p1foff",
        password: "P1FOFF",
        connectString: "tefoff31.ecom.ahold.nl:1540/tefoff31"
    },
    function(err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        dbConnection = connection;
        starupApp();
    });

function starupApp() {
    setInterval(queryDashboardEvents, dashboard_poll_interval);
    setInterval(queryOrderEvents, order_poll_interval);
    setInterval(emitMessage, emit_interval);
}

function emitMessage() {
    if (dashboardEvents) {
        if (dashboardEvents.length > 0) {
            var event = dashboardEvents[0];
            if (event) {
                io.sockets.emit('MapEvent', event);
                dashboardEvents.shift();
            }
        }
    }
}

function queryDashboardEvents() {
    retrieveDashboardEvents(function(events, err) {
        dashboardEvents = events;
    });
}

function queryOrderEvents() {
    retrieveOrderEvents(function(events, err) {
        if (events) {
            for (var i = 0; i < events.length; i++) {
                var event = events[i];
                sendPushMessage(event.device_token, event.event_text, event.event_desc, event.id, function(success) {
                    if (success) {
                        console.log("send push message");
                    } else {
                        console.log("failed to send push message");
                    }
                });
            }
        }
    });
}

app.use(bodyParser.json());

app.get('/order/:user/:order/:id', function(req, res) {
    retrieveOrderEventsForMember(req.params["user"], req.params["order"], req.params["id"], function(event, err) {
        if (event) {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(event));
        } else {
            res.sendStatus(500);
        }
    });
})

app.post('/push/:devicetoken', function(req, res) {
    sendPushMessage(req.params["devicetoken"], req.body["title"], req.body["message"], function(success) {
        if (success) {
            res.sendStatus(200);
        } else {
            res.sendStatus(500);
        }
    });
});

io.on('connection', function(socket) {
    console.log("New socket connection");
});

function doRelease(connection) {
    connection.close(
        function(err) {
            if (err) {
                console.error(err.message);
            }
        });
}

function retrieveDashboardEvents(callback) {
    var query = "with query as (" +
        "select dbd.id, dbd.tijdstip, dbd.activiteit, dbd.naam, dbd.nummer, dbd.bedrag, dbd.lokatie, gps.latitude, gps.longitude " +
        "from hack_dashboard_data dbd, hack_gps gps " +
        "where dbd.lokatie = gps.lokatie (+) " +
        "and dbd.id > :last_id " +
        "order by dbd.id) " +
        "select * from query q where rownum <= 50";

    dbConnection.execute(query, [last_dashboard_id], function(err, result) {
        if (err) {
            console.error(err.message);
            return;
        }

        var events = parseDatabaseResultToArray(result);
        var parsedEvents = [];
        for (var i = 0; i < events.length; i++) {
            var event = events[i];
            var date = new Date(event.tijdstip).getTime() / 1000
            var parsedEvent = new Object();
            parsedEvent.location = new Object();
            parsedEvent.location.long = event.longitude;
            parsedEvent.location.lat = event.latitude;
            parsedEvent.type = event.activiteit;
            parsedEvent.amount = event.bedrag;
            parsedEvent.person = new Object();
            parsedEvent.person.name = event.naam;
            parsedEvent.person.number = event.nummer;
            parsedEvent.date = date;
            parsedEvents.push(parsedEvent);
        }

        if (events.length > 0) {
            last_dashboard_id = events[events.length - 1].id;
            callback(parsedEvents, err);
        } else {
            last_dashboard_id = 0;
            callback(null, err);
        }
    });
}

function retrieveOrderEventsForMember(member, order, id, callback) {
    var query = "select ev.id, ev.member_no, rg.device_token, ev.order_no, ev.event_type, et.description, ev.date_time, et.in_overview_flg, et.notification_flg " +
        "from hack_order_events ev, hack_device_registration rg, hack_event_types et " +
        "where rg.member_no = ev.member_no " +
        "and ev.event_type = et.event_type " +
        "and et.in_overview_flg = 'Y' " +
        "and ev.member_no = :member_no " +
        "order by ev.id"

    dbConnection.execute(query, [member], function(err, result) {
        if (err) {
            console.error(err.message);
            return;
        }

        var events = parseDatabaseResultToArray(result);

        if (events.length > 0) {
            callback(events[events.length - 1], err);
        } else {
            callback(null, err);
        }
    });
}

function retrieveOrderEvents(callback) {
    var query = "select ev.id, ev.member_no, rg.device_token, ev.order_no, ev.event_type, et.description, ev.event_desc, ev.date_time, et.in_overview_flg, et.notification_flg " +
        "from hack_order_events ev, hack_device_registration rg, hack_event_types et " +
        "where rg.member_no = ev.member_no " +
        "and ev.event_type = et.event_type " +
        "and et.notification_flg = 'Y'" +
        "and ev.id > nvl(:last_order_id,0)";

    dbConnection.execute(query, [last_order_id], function(err, result) {
        if (err) {
            console.error(err.message);
            return;
        }

        var events = parseDatabaseResultToArray(result);

        if (events.length > 0) {
            last_order_id = events[events.length - 1].id;
            callback(events, err);
        } else {
            callback(null, err);
        }
    });
}

function parseDatabaseResultToArray(result) {
    var events = []
    for (var i = 0; i < result.rows.length; i++) {
        var row = result.rows[i];
        var object = {}
        for (var b = 0; b < row.length; b++) {
            var property = result.metaData[b]["name"].toLowerCase();
            object[property] = row[b];
        }
        events.push(object);
    }
    return events;
}

function sendPushMessage(deviceId, title, message, id, callback) {
    var devices = [deviceId];

    var service = new apn.Provider({
        cert: "certificates/cert.pem",
        key: "certificates/key.pem",
        production: false,
    });

    var note = new apn.Notification();
    note.title = title;
    note.alert = title;
    note.subtitle = title;
    note.body = message;
    note.payload = {
        "id": id
    };
    note.topic = "com.ahold.test.AppieTabletTime";

    console.log(`Sending: ${note.compile()} to ${devices}`);

    if (debug) {
      console.log("Sending currently disabled");
    } else {
      service.send(note, devices).then(result => {
          console.log("sent:", result.sent.length);
          console.log("failed:", result.failed.length);
          console.log(result.failed);
          if (result.sent.length > 0) {
              callback(true);
          } else {
              callback(false);
          }
      });
    }
}
